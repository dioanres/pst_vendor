<?php

namespace Pst\Pengajuan\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    use HasFactory;

    public $fillable = [
        'attachment',
        'to',
        'amount',
        'status',
        'status_time',
        'app',
        'created_by',
        'updated_by',
        'division_id',
        'filename',
        'detail_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'attachment' => 'string',
        'to' => 'string',
        'amount' => 'integer',
        'status' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'division_id' => 'integer'
    ];

    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'to' => 'required',
        'amount' => 'required',
    ];

    public function history()
	{
		return $this->hasMany(\Pst\Pengajuan\Models\PengajuanHist::class);
    }

    public function detail()
    {
        return $this->hasMany(\Pst\Pengajuan\Models\Pengajuan_Detail::class);
    }
    
    public function status_desc()
	{
		return $this->hasOne(\Pst\Pengajuan\Models\Status::class,'status');
    }

}
