<?php

namespace Pst\Pengajuan\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    
    public $fillable = [
        'status',
        'desc'
    ];

    protected $primaryKey = 'status';

    public function pengajuan()
	{
		return $this->belongsTo(\Pst\Pengajuan\Models\Pengajuan::class);
    }
    
    public function pengajuan_hist()
	{
		return $this->hasMany(\Pst\Pengajuan\Models\PengajuanHist::class);
    }
    
    public static function select()
    {
        return Status::pluck('desc', 'status')->toArray();
    }
}