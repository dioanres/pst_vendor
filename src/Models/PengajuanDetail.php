<?php

namespace Pst\Pengajuan\Models;

use Illuminate\Database\Eloquent\Model;
use DB;



class PengajuanDetail extends Model
{

    public $fillable = [
        'pengajuan_id',
        'desc',
        'amounts',
        'status'
    ];

    public function pengajuan()
    {
        return $this->belongsTo(\Pst\Pengajuan\Models\Pengajuan::class);
    }
}
