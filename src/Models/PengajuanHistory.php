<?php

namespace Pst\Pengajuan\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class PengajuanHist extends Model
{
    
    public $fillable = [
        'pengajuan_id',
        'notes',
        'attachment',
        'from_status',
        'status_pengajuan',
        'to_status',
        'pic'
    ];

    public function pengajuan()
	  {
		return $this->belongsTo(\Pst\Pengajuan\Models\Pengajuan::class);
    }

}
