<?php

namespace Pst\Pengajuan\Facades;

use Illuminate\Support\Facades\Facade;

class Pengajuan extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'pengajuan';
    }
}
